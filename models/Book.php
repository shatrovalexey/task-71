<?php

namespace app\models;

use Yii ;
use yii\db\ActiveRecord ;
use app\models\Author ;

/**
* Книга
*/
class Book extends ActiveRecord {
	/**
	* @param string $tableName - название таблицы в БД
	*/
	protected $tableName = 'book' ;
	protected $primaryKey = [ 'id' ] ;

	/**
	* @return array the validation rules.
	*/
	public function rules( ) {
		return [
			[ [ 'author_id' , 'cyear' , 'title' , 'rating' , ] , 'required' , ] ,
			[ [ 'author_id' , 'cyear' , 'rating' , ] , 'integer' , ] ,
			[ [ 'author_id' , ] , 'exist' , 'skipOnError' => true ,
				'targetClass' => Author::className( ) ,
				'targetAttribute' => [ 'author_id' => 'id' , ] ,
			] ,
		] ;
	}

	/**
	* @return array customized attribute labels
	*/
	public function attributeLabels( ) {
		return [
			'id' => '#' ,
			'author_id' => 'автор' ,
			'title' => 'название' ,
			'cyear' => 'год создания' ,
			'rating' => 'рейтинг' ,
		] ;
	}

	/**
	* Список книг
	*
	* @param integer $author_id - идентификатор автора
	*
	* @return mixed
	*/
	public function getList( $author_id = null ) {
		$query = static::find( )
			->innerJoin( 'author' , 'author.id = book.author_id' ) ;

		if ( $author_id ) {
			$query->where( [
				'author_id' => $author_id ,
			] ) ;
		}

		$pageSize = 10 ;

		$result = [
			'pages' => new \yii\data\Pagination( [
				'totalCount' => $query->count( ) ,
				'pageSize' => $pageSize ,
			] ) ,
			'author' => Author::findOne( $author_id ) ,
		] ;

		$result[ 'page' ] = [
			'count' => ceil( $result[ 'pages' ]->totalCount / $result[ 'pages' ]->limit ) ,
			'current' => intval( \Yii::$app->request->get( $result[ 'pages' ]->pageParam ) ?? 1 ) ,
			'total' => $result[ 'pages' ]->totalCount ,
			'limit' => $result[ 'pages' ]->limit ,
			'offset' => $result[ 'pages' ]->offset ,
		] ;

		if ( $author_id ) {
			$query->orderBy( [ 'cyear' => SORT_ASC , ] ) ;
		} else {
			$query->orderBy( [ 'rating' => SORT_DESC , ] ) ;
		}

		$query->select( [ 'book.*' , 'author.fio AS author_fio' ] )
			->offset( $result[ 'pages' ]->offset )
			->limit( $result[ 'pages' ]->limit ) ;

		$result[ 'book_list' ] = $query->asArray( )->all( ) ;

		return $result ;
	}
}
