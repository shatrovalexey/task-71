<?php

namespace app\models ;

use Yii ;
use yii\db\ActiveRecord ;

/**
* Атор
*/
class Author extends ActiveRecord {
	/**
	* @param string $tableName - название таблицы в БД
	*/
	protected $tableName = 'author' ;

	/**
	* @return array the validation rules.
	*/
	public function rules( ) {
		return [
			[ [ 'fio' , 'slug' , ] , 'required' ] ,
			[ [ 'id' , 'rating' , 'byear' , ] , 'integer' ] ,
		] ;
	}

	/**
	* @return array customized attribute labels
	*/
	public function attributeLabels( ) {
		return [
			'fio' => 'ФИО' ,
			'slug' => 'псевдоним' ,
			'id' => 'идентификатор' ,
			'rating' => 'рейтинг' ,
			'byear' => 'год рождения' ,
		] ;
	}
}
