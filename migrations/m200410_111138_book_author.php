<?php

use yii\db\Migration;

/**
 * Class m200410_111138_book_author
 */
class m200410_111138_book_author extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp( ) {
	$dbh = \Yii::$app->db->masterPdo ;

	$dbh->exec( "
CREATE TABLE IF NOT EXISTS `author`(
	`id` BIGINT( 22 ) UNSIGNED NOT null AUTO_INCREMENT COMMENT 'идентификатор' ,
	`fio` VARCHAR( 100 ) NOT null COMMENT 'ФИО' ,
	`slug` VARCHAR( 100 ) NOT null COMMENT 'псевдоним' ,
	`byear` SMALLINT NOT null COMMENT 'год рождения' ,
	`rating` SMALLINT UNSIGNED NOT null COMMENT 'рэйтинг' ,

	PRIMARY KEY( `id` )
) COMMENT = 'номер' ENGINE = InnoDB CHARSET = utf8mb4 AS
SELECT
	sha1( uuid( ) ) AS `fio` ,
	sha1( uuid( ) ) AS `slug` ,
	year( current_date( ) ) - rand( ) * 4e3 AS `byear` ,
	rand( ) * 100 AS `rating`
FROM
	`mysql`.`user` AS `u1` ,
	`mysql`.`user` AS `u2`
LIMIT 100 ;

CREATE TABLE IF NOT EXISTS `book`(
	`id` BIGINT( 22 ) UNSIGNED NOT null AUTO_INCREMENT COMMENT 'идентификатор' ,
	`author_id` BIGINT( 22 ) UNSIGNED NOT null COMMENT 'идентификатор автора' ,
	`cyear` SMALLINT NOT null COMMENT 'дата публикации' ,
	`title` VARCHAR( 100 ) NOT null COMMENT 'название' ,
	`rating` SMALLINT UNSIGNED NOT null COMMENT 'рэйтинг' ,

	PRIMARY KEY( `id` ) ,
	CONSTRAINT `fk_book_author`
		FOREIGN KEY ( `author_id` )
		REFERENCES `author`( `id` )
		ON DELETE CASCADE
		ON UPDATE CASCADE
) COMMENT = 'номер' ENGINE = InnoDB CHARSET = utf8mb4 AS
SELECT
	`a1`.`id` AS `author_id` ,
	`a1`.`byear` + 15 + rand( ) * 105 AS `cyear` ,
	sha1( uuid( ) ) AS `title` ,
	rand( ) * 100 AS `rating`
FROM
	`author` AS `a1` ,
	`author` AS `a2`
LIMIT 1000 ;
	" ) ;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown( ) {
	$dbh = \Yii::$app->db->masterPdo ;

	$dbh->exec( "
DROP TABLE IF EXISTS `book` ;
DROP TABLE IF EXISTS `author` ;
	" ) ;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200410_111138_number_taken cannot be reverted.\n";

        return false;
    }
    */
}
