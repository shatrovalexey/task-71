<?php
	$this->title = $book->title ;
	$this->params[ 'breadcrumbs' ] = [
		[
			'label' => 'Авторы' ,
			'url' => [ '/' , ] ,
		] ,
		[
			'label' => $author->fio ,
			'url' => [ 'author' , 'id' => $author->id , ] ,
		] ,
		$this->title ,
	] ;
?>
<div>
	<h1>Книга</h1>
	<p>Название: <?=htmlspecialchars( $book->title )?>
	<p>Год издания: <?=htmlspecialchars( $book->cyear )?> год

	<h1>Автор</h1>
	<p>ФИО: <a href="author?id=<?=$author->id?>"><?=htmlspecialchars( $author->fio )?></a>
	<p>Псевдоним: <?=htmlspecialchars( $author->slug )?>
	<p>Год рождения: <?=htmlspecialchars( $author->byear )?> год
</div>