<?php
/* @var $this yii\web\View */

$this->title = 'Книги' ;

$this->params['breadcrumbs'] = [ $this->title , ] ;

?>
<style>
.number_booked_list tbody input {
	width: 100% ;
}
</style>
<div class="site-index">
	<?php if ( $author ) { ?>
	<h2>Автор</h2>
	<p>ФИО: <?=htmlspecialchars( $author->fio )?>
	<p>псевдоним: <?=htmlspecialchars( $author->slug )?>
	<p>год рождения: <?=htmlspecialchars( $author->byear )?>
	<?php } else { ?>
	<p><a href="?">все книги</a>
	<?php } ?>
	<?=\yii\widgets\LinkPager::widget( [ 'pagination' => $pages , ] )?>
	<table class="table" id="number_booked_list">
		<caption><?=htmlspecialchars( $this->title )?></caption>
		<thead>
			<tr>
				<th>#</th>
				<th>Название</th>
				<th>Год публикации</th>
				<th>Автор</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ( $book_list as $book ) { ?>
			<tr>
				<td><?=htmlspecialchars( $book[ 'id' ] )?></td>
				<td>
					<a href="book?id=<?=$book[ 'id' ]?>"><?=htmlspecialchars( $book[ 'title' ] )?></a>
				</td>
				<td><?=htmlspecialchars( $book[ 'cyear' ] )?></td>
				<td>
					<a href="?author_id=<?=$book[ 'author_id' ]?>"><?=htmlspecialchars( $book[ 'author_fio' ] )?></a>
				</td>
			</tr>
			<?php } ?>
		</tbody>
	</table>
	<?=\yii\widgets\LinkPager::widget( [ 'pagination' => $pages , ] )?>
</div>
