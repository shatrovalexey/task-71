<?php
/* @var $this yii\web\View */

$this->title = 'Списки' ;
$this->params['breadcrumbs'] = [
	$this->title ,
] ;
?>
<div class="site-index">
	<div class="body-content">
		<h1><?=htmlspecialchars( $this->title )?></h1>
		<p><a href="site/author-list">Список авторов</a>
		<p><a href="site/book-list">Список книг</a>
	</div>
</div>