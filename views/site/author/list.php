<?php
/* @var $this yii\web\View */

$this->title = 'Авторы' ;
$this->params['breadcrumbs'] = [
	$this->title ,
] ;
?>
<div class="site-index">
	<div class="body-content">
		<table class="table">
			<caption><?=htmlspecialchars( $this->title )?></caption>
			<thead>
				<tr>
					<th>ФИО</th>
					<th>псевдоним</th>
					<th>год рождения</th>
					<th>рэйтинг</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ( $author_list as $author ) { ?>
				<tr>
					<td><a href="site/author?id=<?=$author->id?>"><?=htmlspecialchars( $author->fio )?></a></td>
					<td><?=htmlspecialchars( $author->slug )?></td>
					<td><?=htmlspecialchars( $author->byear )?></td>
					<td><?=htmlspecialchars( $author->rating )?></td>
				</tr>
				<?php } ?>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="3">
						<?=\yii\widgets\LinkPager::widget( [ 'pagination' => $pages , ] )?>
					</td>
				</tr>
			</tfoot>
		</table>
	</div>
</div>
