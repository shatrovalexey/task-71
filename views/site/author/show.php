<?php
$this->title = $author->fio ;
$this->params[ 'breadcrumbs' ] = [
	[
		'label' => 'Авторы' ,
		'url' => [ 'author-list' , ] ,
	] ,
	$this->title ,
] ;
?>
<div>
	<h1>Автор</h1>
	<p>ФИО: <?=htmlspecialchars( $author->fio )?>
	<p>псевдоним: <?=htmlspecialchars( $author->slug )?>
	<p>Год рождения: <?=$author->byear?> год

	<h2>Книги автора</h2>
	<ul>
		<?php foreach ( $book_list as $book ) { ?>
		<li>
			<a href="book?id=<?=$book->id?>" title="название"><?=htmlspecialchars( $book->title )?></a>

			<div>Год издания: <?=htmlspecialchars( $book->cyear )?> год</div>
		</li>
		<?php } ?>
	</ul>
	<?=\yii\widgets\LinkPager::widget( [ 'pagination' => $pages , ] )?>
</div>