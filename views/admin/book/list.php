<?php
/* @var $this yii\web\View */

$this->title = 'Книги' ;

$this->params['breadcrumbs'] = [
	[
		'label' => 'Админка' ,
		'url' => [ '/admin' , ] ,
	] ,
	$this->title ,
] ;

?>
<style>
.number_booked_list tbody input {
	width: 100% ;
}
</style>
<div class="site-index">
	<?php if ( $author ) { ?>
	<h2>Автор</h2>
	<p>ФИО: <?=htmlspecialchars( $author->fio )?>
	<p>псевдоним: <?=htmlspecialchars( $author->slug )?>
	<p>год рождения: <?=htmlspecialchars( $author->byear )?>
	<?php } ?>
	<?=\yii\widgets\LinkPager::widget( [ 'pagination' => $pages , ] )?>
	<p><a href="number-booked-create">забронировать номер</a>
	<table class="table" id="number_booked_list">
		<caption><?=htmlspecialchars( $this->title )?></caption>
		<thead>
			<tr>
				<th>#</th>
				<th>Название</th>
				<th>Год публикации</th>
				<th>Автор</th>
				<th>&nbsp;</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ( $book_list as $book ) { ?>
			<tr>
				<td><?=htmlspecialchars( $book[ 'id' ] )?></td>
				<td><?=htmlspecialchars( $book[ 'title' ] )?></td>
				<td><?=htmlspecialchars( $book[ 'cyear' ] )?></td>
				<td>
					<a href="author-edit?id=<?=$book[ 'author_id' ]?>"><?=htmlspecialchars( $book[ 'author_fio' ] )?></a>
				</td>
				<td>
					<p><a href="book-delete?id=<?=$book[ 'id' ]?>">удалить</a>
					<p><a href="book-edit?id=<?=$book[ 'id' ]?>">редактировать</a>
				</td>
			</tr>
			<?php } ?>
		</tbody>
	</table>
	<?=\yii\widgets\LinkPager::widget( [ 'pagination' => $pages , ] )?>
</div>
