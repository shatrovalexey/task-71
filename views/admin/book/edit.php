<?php
	use yii\helpers\Html ;
	use yii\widgets\ActiveForm ;

	$this->title = 'Редактирование книги' ;

	$this->params['breadcrumbs'] = [
		[
			'label' => 'Админка' ,
			'url' => [ '/admin' , ] ,
		] , [
			'label' => 'Список' ,
			'url' => [ 'book-list' , ] ,
		] ,
		$this->title
	] ;
?>
<h1><?=htmlspecialchars( $this->title . ' #' . $book->id )?></h1>
<?php
	if ( $errors ) {
?>
	<ul>
	<?php foreach( $errors as $error ) { ?>
		<li><?=print_r( $error , true )?></li>
	<?php } ?>
	</ul>
<?php
	}
?>
<?php $form = ActiveForm::begin( ) ; ?>
	<input type="hidden" name="Book[id]" value="<?=$book->id?>">
	<?=$form->field( $book , 'title' )?>
	<?=$form->field( $book , 'cyear' )->textInput( [
		'type' => 'number' ,
	] )?>
	<?=$form->field( $book , 'rating' )->textInput( [
		'type' => 'number' ,
		'max' => 100 ,
		'min' => 0 ,
	] )?>
	<?=$form->field( $book , 'author_id' )
		->dropDownList( \yii\helpers\ArrayHelper::map( $author_list , 'id' , 'fio' ) )?>

	<?=Html::submitButton( \Yii::t( 'app', 'сохранить' ) , [
		'class' => 'btn btn-lg btn-primary btn-block' ,
	] ) ?>
<?php $form->end( ) ; ?>