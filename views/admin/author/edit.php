<?php
	use yii\helpers\Html ;
	use yii\widgets\ActiveForm ;

	$this->title = 'Редактирование автора' ;

	$this->params['breadcrumbs'] = [
		[
			'label' => 'Админка' ,
			'url' => [ '/admin' , ] ,
		] , [
			'label' => 'Список' ,
			'url' => [ 'author-list' , ] ,
		] ,
		$this->title
	] ;
?>
<h1><?=htmlspecialchars( $this->title . ' #' . $author->id )?></h1>
<?php
	if ( $errors ) {
?>
	<ul>
	<?php foreach( $errors as $error ) { ?>
		<li><?=print_r( $error , true )?></li>
	<?php } ?>
	</ul>
<?php
	}
?>
<?php $form = ActiveForm::begin( ) ; ?>
	<?=$form->field( $author , 'id' )->textInput( [
		'type' => 'hidden' ,
	] )?>
	<?=$form->field( $author , 'fio' )?>
	<?=$form->field( $author , 'slug' )?>
	<?=$form->field( $author , 'byear' )->textInput( [
		'type' => 'number' ,
		'max' => date( 'Y' ) ,
	] )?>

	<?=Html::submitButton( \Yii::t( 'app', 'сохранить' ) , [
		'class' => 'btn btn-lg btn-primary btn-block' ,
	] ) ?>
<?php $form->end( ) ; ?>
<h2>Книги автора</h2>
<ul>
	<?php foreach ( $book_list as $book ) { ?>
	<li>
		<a href="book-edit?id=<?=$book->id?>" title="название"><?=htmlspecialchars( $book->title )?></a>
		<div>Год издания: <?=htmlspecialchars( $book->cyear )?> год</div>
	</li>
	<?php } ?>
</ul>
<?=\yii\widgets\LinkPager::widget( [ 'pagination' => $pages , ] )?>