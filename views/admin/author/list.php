<?php
/* @var $this yii\web\View */

$this->title = 'Авторы' ;

$this->params['breadcrumbs'] = [
	[
		'label' => 'Админка' ,
		'url' => [ '/admin' , ] ,
	] ,
	$this->title ,
] ;

?>
<style>
.author_list tbody input {
	width: 100% ;
}
</style>
<div class="site-index">
	<?=\yii\widgets\LinkPager::widget( [ 'pagination' => $pages , ] )?>
	<p><a href="author-create">создать автора</a>
	<table class="table" id="author_list">
		<caption><?=htmlspecialchars( $this->title )?></caption>
		<thead>
			<tr>
				<th width="100">#</th>
				<th>ФИО</th>
				<th>псевдоним</th>
				<th>год рождения</th>
				<th>перейти</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ( $author_list as $author ) { ?>
			<tr>
				<td><?=$author->id?></td>
				<td><?=htmlspecialchars( $author->fio )?></td>
				<td><?=htmlspecialchars( $author->slug )?></td>
				<td><?=htmlspecialchars( $author->byear )?></td>
				<td>
					<p><a href="author-delete?id=<?=$author->id?>">удалить</a>
					<p><a href="author-edit?id=<?=$author->id?>">редактировать</a>
				</td>
			</tr>
			<?php } ?>
		</tbody>
	</table>
	<?=\yii\widgets\LinkPager::widget( [ 'pagination' => $pages , ] )?>
</div>
