<?php
	use yii\helpers\Html ;
	use yii\widgets\ActiveForm ;

	$this->title = 'Создание автора' ;

	$this->params['breadcrumbs'] = [
		[
			'label' => 'Админка' ,
			'url' => [ '/admin' , ] ,
		] , [
			'label' => 'Список' ,
			'url' => [ 'author-list' , ] ,
		] ,
		$this->title
	] ;
?>
<h1><?=htmlspecialchars( $this->title )?></h1>
<?php
	if ( $errors ) {
?>
	<ul>
	<?php foreach( $errors as $error ) { ?>
		<li><?=print_r( $error , true )?></li>
	<?php } ?>
	</ul>
<?php
	}
?>
<?php $form = ActiveForm::begin( ) ; ?>
	<?=$form->field( $author , 'fio' )?>
	<?=$form->field( $author , 'slug' )?>
	<?=$form->field( $author , 'byear' )?>

	<?=Html::submitButton( \Yii::t( 'app', 'сохранить' ) , [
		'class' => 'btn btn-lg btn-primary btn-block' ,
	] ) ?>
<?php $form->end( ) ; ?>