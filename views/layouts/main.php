<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Home', 'url' => ['/site/index']],
            ['label' => 'About', 'url' => ['/site/about']],
            ['label' => 'Contact', 'url' => ['/site/contact']],
            Yii::$app->user->isGuest ? (
                ['label' => 'Login', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>
<div class="right-pane"></div>
<?php $this->endBody() ?>

<script crossorigin src="https://unpkg.com/react@16/umd/react.production.min.js"></script>
<script crossorigin src="https://unpkg.com/react-dom@16/umd/react-dom.production.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/babel-standalone/6.25.0/babel.min.js"></script>

<script type="text/babel">
class BookList extends React.Component {
	constructor( props ) {
		super( props ) ;
		this.state = {
			error: null ,
			isLoaded: false ,
			items: [ ] ,
			pages: { }
		} ;
	}

	nextPage( page ) {
		this.setSate( {
			"isLoaded" : false
		} ) ;

		fetch( "/api/book-list?page=" + page + "&per-page=5" )
			.then( res => res.json( ) )
			.then(
				( result ) => {
					this.setState( {
						"isLoaded": true ,
						"items": result.book_list ,
						"pages" : result.page.pages
					} ) ;
				} ,
				( error ) => {
					this.setState( {
						"isLoaded": true ,
						error
					} ) ;
				}
			)
	}

	componentDidMount( ) {
		this.nextPage( 1 )
	}

	render( ) {
		const { error , isLoaded , items , pages } = this.state ;

		if ( error ) {
			return <div class="message-error">Ошибка: {error.message}</div> ;
		}
		if ( ! isLoaded ) {
			return <div class="message-loading">Загрузка...</div> ;
		}

		return ( <div clas="right-pane-body">
			<ul class="right-pane-pages">
				<li>
					<h3>страницы</h3>
				</li>
				{pages.map( page => (
					<li key={page.value} onClick={( ) => this.nextPage(page.value)} class={page.selected}>{page.value}</li>
				) ) }
			</ul>
			<ul class="both right-pane-books">
				<li>
					<h3>книги</h3>
				</li>
				{items.map( item => (
					<li key={item.id} class="right-pane-book">
						<p><a href="book?id={item.id}" class="book_list-title" title="название">{item.title}</a></p>
						<p><a href="author?id={item.author_id}" class="book_list-author_fio" title="автор">{item.author_fio}</a></p>
					</li>
				) )}
			</ul>
		</div> ) ;
	}
}

ReactDOM.render( (
	<div><BookList/></div>
) , document.querySelector( ".right-pane" ) )
</script>
<style>
.both {
	clear: both ;
}
.right-pane {
	opacity: 0.5 ;
	position: fixed ;
	bottom: 0 ;
	right: 0 ;
	max-width: 30% ;
	overflow-y: auto ;
	overflow-x: hidden ;
	max-height: calc( 100vh - 50px ) ;
	background-color: #ffffff ;
	border: 1px silver solid ;
	padding: 5px ;
}
.right-pane-book {
	border: 1px silver solid ;
	padding: 2px ;
	border-radius: 2px ;
	cursor: pointer ;
}
.right-pane-book:hover {
	background-color: LightYellow ;
}
.book_list-author_fio {
	color: #000000 ;
}
.right-pane-pages li {
	display: block ;
	cursor: pointer ;
	margin: 2px ;
	float: left ;
}
.right-pane-pages li.selected ,
.right-pane-pages li:hover {
	color: red ;
}
.right-pane , 
.right-pane li ,
.right-pane-books ,
.right-pane-books li ,
right-pane-pages ,
right-pane-pages li {
	padding-left: 0 ;
	list-style-type: none ;
}
.right-pane:hover {
	opacity: 1.0 ;
}
</style>
</body>
</html>
<?php $this->endPage() ?>
