<?php
namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Author ;
use app\models\Book ;
use app\models\User ;
use yii\db\Query ;

/**
* API
*/
class ApiController extends Controller {
	/**
	* @internal
	*/
	public function beforeAction( $action ) {
		$result = parent::beforeAction( $action ) ;
		\Yii::$app->response->format = Response::FORMAT_JSON ;

		header( 'Access-Control-Allow-Origin: *' ) ;

		return $result ;
	}

	/**
	* Список книг
	*
	* @param integer $author_id - идентификатор автора
	*
	*/
	public function actionBookList( ) {
		$author_id = \Yii::$app->request->get( 'author_id' ) ;

		$result = Book::getList( $author_id ) ;

		$result[ 'page' ][ 'pages' ] = range( 1 , $result[ 'page' ][ 'count' ] ) ;

		foreach ( $result[ 'page' ][ 'pages' ] as &$page ) {
			$page = [
				'value' => "$page" ,
				'selected' => ( $page == $result[ 'page' ][ 'current' ] ) ? 'selected' : null ,
			] ;
		}

		return $result ;
	}
}