<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Author ;
use app\models\Book ;
use app\models\User ;
use yii\db\Query ;

class AdminController extends Controller {
    /**
     * {@inheritdoc}
     */
    public function behaviors( ) {
        return [
            'access' => [
                'class' => AccessControl::className( ) ,
                'only' => [ 'logout' , 'signup' , ] ,
                'rules' => [
                    [
                        'actions' => [ 'signup' , 'login' , ] ,
                        'allow' => true ,
                        'roles' => [ '?' ] ,
                    ] , [
                        'actions' => [ 'logout' , ] ,
                        'allow' => true ,
                        'roles' => [ '@' ] ,
                    ] ,
                ] ,
            ] ,
            'verbs' => [
                'class' => VerbFilter::className( ) ,
                'actions' => [
			'logout' => [ 'post' ] ,
                ] ,
            ] ,
        ] ;
    }

	public function beforeAction( $action ) {
		if (
			! in_array( $action->actionMethod , [ 'actionLogin' ] ) &&
			( \Yii::$app->user->identity->role_id != User::ROLE_ADMIN ) ||
			! parent::beforeAction( $action )
		) {
			return $this->goHome( ) ;
		}

		return true ;
	}

    /**
     * {@inheritdoc}
     */
    public function actions( ) {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

	/**
	* Главная страница
	*/
	public function actionIndex( ) {
		return $this->render( 'index' ) ;
	}

	/**
	* Список номеров
	*/
	public function actionAuthorList( ) {
		$query = Author::find( ) ;

		$totalCount = $query->count( ) ;

		$pages = new \yii\data\Pagination( [
			'totalCount' => $totalCount ,
			'pageSize' => 10 ,
		] ) ;

		$author_list = $query
				->orderBy( [ 'id' => SORT_DESC , ] )
				->offset( $pages->offset )
				->limit( $pages->limit )->all( ) ;

		return $this->render( 'author/list' , [
			'pages' => $pages ,
			'author_list' => $author_list ,
		] ) ;
	}

	/**
	* Удалить автора
	*
	* @param integer $id - идентификатор автора
	*/
	public function actionAuthorDelete( $id ) {
		Author::findOne( $id )->delete( ) ;

		return $this->redirect( \Yii::$app->request->referrer ) ;
	}

	/**
	* Создать автора
	*/
	public function actionAuthorCreate( ) {
		$author = new Author( ) ;
		$data = \Yii::$app->request->post( ) ;

		if ( $author->load( $data ) && $author->validate( ) ) {
			$author->save( ) ;

			return $this->render( 'author/edit' , [
				'author' => $author ,
				'errors' => [ ] ,
			] ) ;
		}

		return $this->render( 'author/create' , [
			'errors' => $author->getErrors( ) ,
			'author' => $author ,
		] ) ;
	}

	/**
	* Редактировать номер
	*
	* @param integer $id - идентификатор номера
	*/
	public function actionAuthorEdit( $id ) {
		$model = new Author( ) ;
		$data = \Yii::$app->request->post( ) ;

		if ( $model->load( $data ) && $model->validate( ) ) {
			$author = Author::findOne( $model->id ) ;
			$author->fio = $model->fio ;
			$author->slug = $model->slug ;
			$author->byear = $model->byear ;
			$author->update( ) ;
		}

		if ( empty( $author ) ) {
			$author = Author::findOne( $id ) ;
		}

		$query = Book::find( )->where( [
			'author_id' => $author->id ,
		] ) ;

		$pages = new \yii\data\Pagination( [
			'totalCount' => $query->count( ) ,
			'pageSize' => 10 ,
		] ) ;

		$book_list = $query->limit( $pages->limit )->offset( $pages->offset )->all( ) ;

		return $this->render( 'author/edit' , [
			'pages' => $pages ,
			'errors' => $model->getErrors( ) ,
			'author' => $author ,
			'book_list' => $book_list ,
		] ) ;
	}

	/**
	* Список книг
	*
	* @param integer $author_id - идентификатор автора
	*/
	public function actionBookList( $author_id = null ) {
		$author = Author::findOne( $author_id ) ;
		$query = Book::find( )
			->innerJoin( 'author' , 'book.author_id=author.id' ) ;

		if ( $author_id ) {
			$query->where( [
				'book.author_id' => $author_id ,
			] ) ;
		}

		$totalCount = $query->count( ) ;

		$pages = new \yii\data\Pagination( [
			'totalCount' => $totalCount ,
			'pageSize' => 10 ,
		] ) ;

		if ( $author_id ) {
			$query->orderBy( [ 'book.cyear' => SORT_ASC , ] ) ;
		} else {
			$query->orderBy( [ 'book.rating' => SORT_DESC , ] ) ;
		}

		$query->offset( $pages->offset )
			->limit( $pages->limit )
			->select( [ 'book.*' , 'author.fio AS author_fio' ] ) ;

		return $this->render( 'book/list' , [
			'pages' => $pages ,
			'book_list' => $query->asArray( )->all( ) ,
			'author' => $author ,
		] ) ;
	}

	/**
	* Создать книгу
	*/
	public function actionBookCreate( ) {
		$book = new Book( ) ;
		$author_list = Author::find( )->all( ) ;
		$data = \Yii::$app->request->post( ) ;

		if ( $book->load( $data ) && $book->validate( ) ) {
			$book->save( ) ;

			return $this->render( 'number_booked/edit' , [
				'book' => $book ,
				'author_list' => $author_list ,
				'errors' => [ ] ,
			] ) ;
		}

		return $this->render( 'number_booked/create' , [
			'errors' => $book->getErrors( ) ,
			'book' => $book ,
			'author_list' => $author_list ,
		] ) ;
	}

	/**
	* Редактировать книгу
	*
	* @param integer $id - идентификатор
	*/
	public function actionBookEdit( $id ) {
		$model = new Book( ) ;
		$data = \Yii::$app->request->post( ) ;

		if ( $model->load( $data ) && $model->validate( ) ) {
			$book = Book::findOne( $id ) ;
			$book->title = $model->title ;
			$book->cyear = $model->cyear ;
			$book->author_id = $model->author_id ;
			$book->update( ) ;
		}

		if ( empty( $book ) ) {
			$book = Book::findOne( $id ) ;
		}

		return $this->render( 'book/edit' , [
			'errors' => $model->getErrors( ) ,
			'book' => $book ,
			'author_list' => Author::find( )->all( ) ,
		] ) ;
	}

	/**
	* Удалить книгу
	*
	* @param integer $id - идентификатор
	*/
	public function actionBookDelete( $id ) {
		Book::findOne( $id )->delete( ) ;

		return $this->redirect( \Yii::$app->request->referrer ) ;
	}

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
