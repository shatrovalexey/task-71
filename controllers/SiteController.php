<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Author ;
use app\models\Book ;
use app\models\User ;
use yii\db\Query ;

class SiteController extends Controller {
    /**
     * {@inheritdoc}
     */
    public function behaviors( ) {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions( ) {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

	/**
	* Главная страница
	*/
	public function actionIndex( ) {
		return $this->render( 'index' ) ;
	}

	/**
	* Список авторов
	*/
	public function actionAuthorList( ) {
		$query = Author::find( ) ;
		$pages = new \yii\data\Pagination( [
			'totalCount' => $query->count( ) ,
			'pageSize' => 10 ,
		] ) ;
		$author_list = $query
			->orderBy( [ 'rating' => SORT_DESC , ] )
			->offset( $pages->offset )->limit( $pages->limit )->all( ) ;

		return $this->render( 'author/list' , [
			'pages' => $pages ,
			'author_list' => $author_list ,
		] ) ;
	}

	/**
	* Список книг
	*
	* @param integer $author_id - идентификатор автора
	*/
	public function actionBookList( $author_id = null ) {
		return $this->render( 'book/list' , Book::getList( $author_id ) ) ;
	}

	/**
	* Страница автора
	*
	* @param integer $id - идентификатор автора
	*/
	public function actionAuthor( $id ) {
		$author = Author::findOne( $id ) ;
		$book_list = Book::find( )->where( [
			'author_id' => $id ,
		] ) ;

		$pages = new \yii\data\Pagination( [
			'totalCount' => $book_list->count( ) ,
			'pageSize' => 10 ,
		] ) ;

		$book_list
			->orderBy( [ 'cyear' => SORT_ASC , ] )
			->offset( $pages->offset )->limit( $pages->limit ) ;

		return $this->render( 'author/show' , [
			'pages' => $pages ,
			'author' => $author ,
			'book_list' => $book_list->all( ) ,
		] ) ;
	}

	/**
	* Страница книги
	*
	* @param integer $id - идентификатор книги
	*/
	public function actionBook( $id ) {
		$book = Book::findOne( $id ) ;
		$author = Author::findOne( $book->author_id ) ;

		return $this->render( 'book/show' , [
			'author' => $author ,
			'book' => $book ,
		] ) ;
	}

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ( $model->load( Yii::$app->request->post( ) ) && $model->login( ) ) {
		if ( \Yii::$app->user->identity->role_id == User::ROLE_ADMIN ) {
			return $this->redirect( '/admin' ) ;
		}

		return $this->goBack( ) ;
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
